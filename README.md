# Pre packaged dependencies for maven

This git repo contains a maven directory with all the dependencies needed to run the exercices
To install it in your maven configuration :
- first download it and unzip it
- find the .m2 directory on your pc or laptop : most of the time it is located in C:\Users\<your_user>\.m2 (windows), or /home/<your_users>/.m2 (mac)
- copy the content of the directory "repository" (downloaded from gitlab) into C:\Users\<your_user>\.m2\repository